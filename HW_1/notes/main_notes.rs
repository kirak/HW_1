//! Command-line modular exponentiation tool - Notated version for study
//!
//! Kira Klingenberg
//! Programming in Rust - Spring 2023

//Main code Citations:
//https://internals.rust-lang.org/t/mathematical-modulo-operator/5952
//https://cheats.rs/#type-conversions
//https://doc.rust-lang.org/rust-by-example/conversion/from_into.html
//https://dev.to/brunooliveira/learning-rust-understanding-vectors-2ep4#:~:text=In%20Rust%2C%20there%20are%20several%20ways%20to%20initialize%20a%20vector.&text=In%20order%20to%20initialize%20a,%2C%20empty%20Vec%20.

use std::env;
use std::str::FromStr; //FromStr is a trait, and this brings it into scope so we can use it
// This module provies some functions and types we need for CLA

//use unsigned to control type to be non negative
//use 64 size to ensure all inputs are less than 2**64
//! Parse command line arguments and call modexp
fn main() {
    let mut numbers = Vec::new(); //dynamic list to hold CLA

    //process CLA
    for arg in env::args().skip(1) {
        numbers.push(u64::from_str(&arg).expect("error parsing"));
    }

    //check we got real data
    if numbers.is_empty() {
        // if numbers.len() == 0 {
        eprintln!("No data provided.");
        std::process::exit(1); //exit program on err
    }

    //push a spot onto numbers to hold a result at index 3
    numbers.push(0u64);

    //now we have the 3 values needed from CLA in the numebrs vector
    //and we have a value to represent the final result of the calculation
    //we can access them via numbers[0] indexing
    //lend numbers to the modexp function
    let result = modexp(&numbers);
    println!("The result is: {}", result);
}

//! Calculates modular exponentiaion from a vector of values
//borrows a reference to a u64 Vector, numbers
fn modexp(numbers: &[u64]) -> u64 {
    //clippy warning changed from &Vec <u64>
    //get info from the numbers vector and store as u128 types
    let mut x: u128 = u128::from(numbers[0]);
    let mut y: u128 = u128::from(numbers[1]);
    let m: u128 = u128::from(numbers[2]);
    let mut result: u128 = u128::from(numbers[3]);

    assert!(m != 0 && !(x == 0 && y == 0), "Invalid data provided.");

    //overflowing_<operation> returns a tuple: (result value, bool for if overflow or not)
    let overflow = (m - 1).overflowing_pow(2);

    if m != 1 && !overflow.1 {
        //process the data and perform the mod
        result = 1; //result gets set to 1
        while y > 0 {
            if y % 2 == 1 {
                result = (x * result) % m;
            }
            y /= 2;
            x = x.pow(2) % m //compiler suggestion to use .pow
        }
    }
    //otherwise result should stay as 0
    //result will always fall in u64 range if check for overflow above passes
    let u64_result: u64 = u64::try_from(result).unwrap();
    u64_result
}

//run tests with 'cargo test'
//Test Citation: https://doc.rust-lang.org/rust-by-example/testing/unit_testing.html
//! Tests for modexp
#[cfg(test)]
mod test_modexp {
    use super::*; //don't fully understand this yet, pulled from above citation

    //! test large values on modexp
    #[test]
    fn test_bigm() {
        //Dr.Massey's provided tests, converted to use vector that includes result at index 3, below:
        let bigm = u64::max_value() - 58;

        let test1 = vec![bigm - 2, bigm - 1, 1, 0];
        assert_eq!(0, modexp(&test1));

        //TODO these tests fail here, but pass in should_panic
        let test2 = vec![bigm - 2, bigm - 1, bigm, 0];
        assert_eq!(1, modexp(&test2));
        let test3 = vec![bigm - 2, (1 << 32) + 1, bigm, 0];
        assert_eq!(827419628471527655, modexp(&test3));
    }

    //! Test regular values on modexp
    #[test]
    fn test_values() {
        //First three are Bart's provided tests
        let test4 = vec![10, 9, 6, 0];
        assert_eq!(4, modexp(&test4));

        let test5 = vec![450, 768, 517, 0];
        assert_eq!(34, modexp(&test5));

        let test6 = vec![2, 20, 17, 0];
        assert_eq!(16, modexp(&test6));

        //new tests below:
        let test7 = vec![80, 33, 12, 0];
        assert_eq!(8, modexp(&test7));

        let test8 = vec![2, 2, 2, 0];
        assert_eq!(0, modexp(&test8));

        let test9 = vec![2, 0, 2, 0];
        assert_eq!(1, modexp(&test9));
    }

    //!Test values that cause modexp to panic
    #[test]
    #[should_panic]
    fn test_panics() {
        let bigm = u64::max_value() - 58;

        //These two are Barts. They panic because (m-1)^2 won't fit in a u64
        let test2 = vec![bigm - 2, bigm - 1, bigm, 0];
        assert_eq!(1, modexp(&test2));
        let test3 = vec![bigm - 2, (1 << 32) + 1, bigm, 0];
        assert_eq!(827419628471527655, modexp(&test3));

        let missing_arg = vec![2, 10, 4];
        modexp(&missing_arg);

        let no_data = vec![];
        modexp(&no_data);

        let xy_0 = vec![0, 0, 2, 0];
        modexp(&xy_0);

        let m_is_0 = vec![2, 33, 0, 0];
        modexp(&m_is_0);
    }
}
