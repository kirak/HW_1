# Kira Klingenberg - HW1

Before starting the homework I took Bart's advice and followed the examples in the book as I took notes.
The GDC example in Chapter 2 was really helpful as a model for the modexp CLA parsing we needed for modexp. 
I decided to parse the CLA's into a Vector, just as the book did, because I wanted to practice working with Vectors in Rust.
The parsing itself was pretty straightforward, as I followed the books GDC pretty closely. 
I didn't end up needing the parse() method Bart referred to in the instructions, so I hope that's ok.
One thing I am still fuzzy on, is if the .expect actually panics as well as prints the error message. 
It appears to do so, as I wrote in a test that should panic when one argument is missing, but I'm not 100% sure I traced it back to the right thing.

One design choice I'm not feeling too great about, is how to return the result back from the modexp function.
I initially thought I would push a value representing the result, to the numbers Vector, and change that value as the function did it's thing.
This proved to be more difficult that I realized, and I think I will have to return to this idea once I better understand how to mutate a vector element using a borrowed reference.
So instead I simply extracted the initialized '0' value for the result into a u128, and made sure to store the result and have the function return what was in result at the end.
I left the result element in the Vector, because if I have time I'd like to return to the idea of storing and updating the result into the Vector itself using the borrowed reference.

The calculation itself was pretty straightforward once I had extracted all the args from the numbers vector.
I just followed along with the provided psuedocode. 

A big challenge was figuring out how to check if (m-1)^2 would overflow a u64. 
I am still working on doing this correctly, but my intuition said: try the calculation (m-1)^2 and store the result in a u128.
Then use the unwrap and try_from methods from the instructions to see if we can fit that result back into a u64. 
I asked about this on Zulip, so hopefully I'll have an update about what I decided to do before submitting.
EDIT: I ended up using the overflowing_pow function to ensure the calculation would only run if an overflow didn't happen.

Testing went well, I like Rust's unit test structure. 
It's pretty similar to other languages unit tests, but is nice and streamlined to be able to fit right next to the function itself, which is cool.
I am not sure why the two tests Bart provided where m is the value of bigm fail unless they are in the should_panic section.
I suspect it is because indeed, the value for m in these tests will overflow a u64 when (m-1)^2.
But I feel a little big wary, since I assume Bart's provided tests were suposed to pass as is. 
Perhaps I didn't do the overflow check right after all? We'll see...
EDIT: fixed! I was doing the overflow check wrong

This was a good assignment, I learned a lot.
I had to do a fair amount of googling (citation at the top of the file), but for the most part the book had what I needed.
:)

4/16/2023 I made some changes on my laptop while away this week, but messed up the git tracking. So I am sending myself the updated main.rs file and doing a dreaded copy-paste to bring in the changes. 
I don't like doing it this way and will get things fixed for future assigments. This is a one off.

Also, clippy is giving me a warning that the crate name "HW_1" should be snake case, but I'm not finding any way to rename the crate without making a copy. 
So this will be a persistent clippy warning. 
In the future I'll make sure my crate names are lowercase snakecase.
I believe all other clippy warning are resolved, as are any formatting issues.

When creating my vector from the inputs, my parser will panic if any of the inputs are negative, since my vectors are an unsigned type. 
I wasn't able to figure out how to write a test creating a vector that held a negative number and not cause the compiler to yell due to type mismatches.
Conceptually, I am thinking the way to do it would be to test for the actual CLA inputs, rather than the resulting vector values, but I'm not sure how to do it.
I'll make a note and revisit this later, but likely won't get to it before I submit.