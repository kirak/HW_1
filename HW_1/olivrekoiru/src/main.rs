//! Command-line modular exponentiation tool
//!
//! Kira Klingenberg
//! Programming in Rust - Spring 2023

//Main code Citations:
//https://internals.rust-lang.org/t/mathematical-modulo-operator/5952
//https://cheats.rs/#type-conversions
//https://doc.rust-lang.org/rust-by-example/conversion/from_into.html
//https://dev.to/brunooliveira/learning-rust-understanding-vectors-2ep4#:~:text=In%20Rust%2C%20there%20are%20several%20ways%20to%20initialize%20a%20vector.&text=In%20order%20to%20initialize%20a,%2C%20empty%20Vec%20.

use std::env;
use std::str::FromStr;

/// Parse command line arguments and call modexp
fn main() {
    let mut numbers = Vec::new();

    //process CLA
    for arg in env::args().skip(1) {
        numbers.push(u64::from_str(&arg).expect("error parsing"));
    }

    //check we got data
    if numbers.is_empty() {
        eprintln!("No data provided.");
        std::process::exit(1);
    }

    //push a spot onto numbers to hold a result value at index 3
    numbers.push(0u64);

    //let modexp borrow 'numbers'
    let result = modexp(&numbers);
    println!("The result is: {}", result);
}

/// Calculates modular exponentiaion from a vector of values
//clippy suggestion: changed type from &Vec <u64>
fn modexp(numbers: &[u64]) -> u64 {
    //convert values to u128 types
    let mut x: u128 = u128::from(numbers[0]);
    let mut y: u128 = u128::from(numbers[1]);
    let m: u128 = u128::from(numbers[2]);
    let mut result: u128 = u128::from(numbers[3]);

    assert!(m != 0 && !(x == 0 && y == 0), "Invalid data provided.");

    //overflowing_pow returns a tuple: (result value, bool for if overflow or not)
    let overflow = (m - 1).overflowing_pow(2);

    if m != 1 && !overflow.1 {
        //perform calculation
        result = 1;
        while y > 0 {
            if y % 2 == 1 {
                result = (x * result) % m;
            }
            y /= 2;
            x = x.pow(2) % m //compiler suggestion to use .pow
        }
    }
    //otherwise result should stay as 0
    //result will always fall in u64 range if check for overflow above passes
    let u64_result: u64 = u64::try_from(result).unwrap();
    u64_result
}

//run tests with 'cargo test'
//Test Setup Citation: https://doc.rust-lang.org/rust-by-example/testing/unit_testing.html
/// Tests for modexp
#[cfg(test)]
mod test_modexp {
    use super::*;

    #[test]
    fn test_bigm() {
        //Below are Dr.Massey's provided tests, converted to use vector that includes result at index 3
        let bigm = u64::max_value() - 58;

        let test1 = vec![bigm - 2, bigm - 1, 1, 0];
        assert_eq!(0, modexp(&test1));

        let test2 = vec![bigm - 2, bigm - 1, bigm, 0];
        assert_eq!(1, modexp(&test2));

        let test3 = vec![bigm - 2, (1 << 32) + 1, bigm, 0];
        assert_eq!(827419628471527655, modexp(&test3));
    }

    #[test]
    fn test_values() {
        //First three are Dr.Massey's provided tests
        let test4 = vec![10, 9, 6, 0];
        assert_eq!(4, modexp(&test4));

        let test5 = vec![450, 768, 517, 0];
        assert_eq!(34, modexp(&test5));

        let test6 = vec![2, 20, 17, 0];
        assert_eq!(16, modexp(&test6));

        //new tests below:
        let test7 = vec![80, 33, 12, 0];
        assert_eq!(8, modexp(&test7));

        let test8 = vec![2, 2, 2, 0];
        assert_eq!(0, modexp(&test8));

        let test9 = vec![2, 0, 2, 0];
        assert_eq!(1, modexp(&test9));
    }

    #[test]
    #[should_panic]
    fn test_panics() {
        let bigm = u64::max_value() - 58;

        //These two are Dr.Massey's. They panic because (m-1)^2 won't fit in a u64
        let test2 = vec![bigm - 2, bigm - 1, bigm, 0];
        assert_eq!(1, modexp(&test2));
        let test3 = vec![bigm - 2, (1 << 32) + 1, bigm, 0];
        assert_eq!(827419628471527655, modexp(&test3));

        let missing_arg = vec![2, 10, 4];
        modexp(&missing_arg);

        let no_data = vec![];
        modexp(&no_data);

        let xy_0 = vec![0, 0, 2, 0];
        modexp(&xy_0);

        let m_is_0 = vec![2, 33, 0, 0];
        modexp(&m_is_0);
    }
}
